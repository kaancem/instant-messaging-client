package com.example.sahilliolu.myapplication;

/**
 * Created by sahillioğlu on 27.11.2016.
 */


import  android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class EndDB extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="mytest.db";
    public static final String TABLE_MESSAGE="mytest";
    public static final String COLUMN_MESSAGE="message";
    public static final String COLUMN_JID="jid";
    public static final String COLUMN_TIME="time";
    public static final String COLUMN_STATE="state";
    public static final String COLUMN_ID="_id";
    public static final String COLUMN_HOST="host";
    private static final String TAG ="EndDB";



    public EndDB(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query="CREATE TABLE " +  TABLE_MESSAGE + "("+ COLUMN_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+ COLUMN_JID+ " TEXT, "+ COLUMN_MESSAGE +" TEXT, " +COLUMN_STATE +" TEXT, " +COLUMN_TIME +" INTEGER, " + COLUMN_HOST+" TEXT "+" );";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS "+ TABLE_MESSAGE);
        onCreate(sqLiteDatabase);
    }

    public void addnewMessage(Messages message){
        ContentValues values = new ContentValues();
        values.put(COLUMN_JID,message.get_jid());
        values.put(COLUMN_MESSAGE,message.get_message());
        values.put(COLUMN_STATE,message.get_state());
        values.put(COLUMN_TIME,message.getTime());
        values.put(COLUMN_HOST,message.get_host());
        Log.d(TAG, "My infos are "+ message.get_state()+" "+message.getTime()+" "+message.get_host()+" is my host"  );
        SQLiteDatabase db=getWritableDatabase();
        long result=db.insert(TABLE_MESSAGE,null,values);
        if(result>0){
            Log.d(TAG,"Success in adding");
        }else{
            Log.d(TAG, "Fail while inserting");
        }
        db.close();
    }
    public String[] displayMessage(String[] returnthis){
        SQLiteDatabase db=getReadableDatabase();
        String query="SELECT   "+ COLUMN_MESSAGE + " c  FROM "+ TABLE_MESSAGE + " WHERE 1" ;
        Cursor c=db.rawQuery(query,null);
        c.moveToFirst();
        int i=0;
        while(!c.isAfterLast()){
            returnthis[i]=c.getString(0);
            i++;
            c.moveToNext();
        }
        db.close();
        return returnthis;
    }
    public void holdMessage(List<Messages> container){
        SQLiteDatabase db=getReadableDatabase();
        String query="SELECT  *   FROM "+ TABLE_MESSAGE + " WHERE 1" ;
        Cursor c=db.rawQuery(query,null);
        c.moveToFirst();
        int i=0;
        while(!c.isAfterLast()){
            container.add(new Messages(c.getString(1),c.getString(2),c.getString(3),c.getLong(4),c.getString(5)));
            Log.d(TAG, c.getString(1)+" Mail");
            Log.d(TAG, c.getString(2)+" Mesaj");
            Log.d(TAG, c.getString(3)+" State");
            Log.d(TAG, c.getLong(4)+" Time");
            Log.d(TAG, c.getString(5)+" Host");
            i++;
            c.moveToNext();
        }
        db.close();
    }
    public void delete(){
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_MESSAGE+" WHERE 1;");
    }
}
