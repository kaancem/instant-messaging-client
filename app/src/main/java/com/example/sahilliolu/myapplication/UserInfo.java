package com.example.sahilliolu.myapplication;

/**
 * Created by sahillioğlu on 27.11.2016.
 */


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class UserInfo extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION=1;
    private static final String DATABASE_NAME="userinfo.db";
    public static final String TABLE_MESSAGE="userinfo";
    public static final String COLUMN_JID="jid";
    public static final String COLUMN_ID="_id";
    private static final String TAG ="TestDB";



    public UserInfo(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String query="CREATE TABLE " +  TABLE_MESSAGE + "("+ COLUMN_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT, "+ COLUMN_JID+ " TEXT" +" );";
        sqLiteDatabase.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(" DROP TABLE IF EXISTS "+ TABLE_MESSAGE);
        onCreate(sqLiteDatabase);
    }

    public void addnewMessage(String message){
        ContentValues values = new ContentValues();
        values.put(COLUMN_JID,message);
        SQLiteDatabase db=getWritableDatabase();
        long result=db.insert(TABLE_MESSAGE,null,values);
        if(result>0){
            Log.d(TAG,"Success in adding username");
        }else{
            Log.d(TAG, "Fail while inserting username");
        }
        db.close();
    }
    public String[] displayMessage(String[] returnthis){
        SQLiteDatabase db=getReadableDatabase();
        String query="SELECT   "+ COLUMN_JID + " c  FROM "+ TABLE_MESSAGE + " WHERE 1" ;
        Cursor c=db.rawQuery(query,null);
        c.moveToFirst();
        int i=0;
        while(!c.isAfterLast()){
            returnthis[i]=c.getString(0);
            i++;
            c.moveToNext();
        }
        db.close();
        return returnthis;
    }
    public String[] holdMessage(String[] container){
        SQLiteDatabase db=getReadableDatabase();
        String query="SELECT  *   FROM "+ TABLE_MESSAGE + " WHERE 1" ;
        Cursor c=db.rawQuery(query,null);
        c.moveToFirst();
        int i=0;
        while(!c.isAfterLast()){
            container[i]=c.getString(1);
            Log.d(TAG, c.getString(1)+" userName");


            i++;
            c.moveToNext();
        }
        db.close();
        return container;
    }
    public void delete(){
        SQLiteDatabase db=getWritableDatabase();
        db.execSQL("DELETE FROM "+TABLE_MESSAGE+" WHERE 1;");
    }
}
