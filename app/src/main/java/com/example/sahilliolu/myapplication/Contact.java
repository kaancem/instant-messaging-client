package com.example.sahilliolu.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sahillioğlu on 17.11.2016.
 */

public class Contact implements Parcelable {
    private String jid;
    private String realName;
    private String userName;
    private boolean isOnline=false;
    public Contact(String jid, String userName, String realName) {
        this.jid = jid;
        this.userName = userName;
        this.realName = realName;
    }

    public Contact(String jid, String userName) {
        this.jid = jid;
        this.userName = userName;
    }

    protected Contact(Parcel in){
        this.jid = in.readString();
        this.realName = in.readString();
        this.userName = in.readString();
        this.isOnline = in.readByte() != 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int i) {
        dest.writeString(jid);
        dest.writeString(realName);
        dest.writeString(userName);
        dest.writeByte(this.isOnline ? (byte) 1 : (byte) 0);
    }

    public Contact(String contactJid )
    {
        jid = contactJid;
    }

    public Contact withOnlineStatus(boolean isOnline){
        Contact newContact = new Contact(this.getJid(), this.getUserName(), this.getRealName());
        newContact.setIsOnline(isOnline);
        return newContact;
    }

    public String getJid()
    {
        return jid;
    }

    public void setJid(String jid) {
        this.jid = jid;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public boolean getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(boolean isOnline) {
        this.isOnline = isOnline;
    }

    public static final Creator<Contact> CREATOR = new Creator<Contact>() {
        @Override
        public Contact createFromParcel(Parcel source) {
            return new Contact(source);
        }

        @Override
        public Contact[] newArray(int size) {
            return new Contact[size];
        }
    };


}
