package com.example.sahilliolu.myapplication;

/**
 * Created by sahillioğlu on 27.11.2016.
 */

public class Messages {
    private String _message;
    private String _jid;
    private int _id;
    private String _state;
    private Long _time;

    public String get_host() {
        return _host;
    }

    public void set_host(String _host) {
        this._host = _host;
    }

    private String _host;
    public String get_state() {
        return _state;
    }

    public void set_state(String state) {
        this._state = state;
    }

    public Long getTime() {
        return this._time;
    }

    public void setTime(Long time) {
        this._time = time;
    }

    private Long Time;

    public String get_contactjid() {
        return _contactjid;
    }

    public void set_contactjid(String _contactjid) {
        this._contactjid = _contactjid;
    }

    private String _contactjid;

    public String get_message() {
        return _message;
    }

    public Messages() {

    }

    public void set_message(String _message) {
        this._message = _message;
    }

    public String get_jid() {
        return _jid;
    }

    public void set_jid(String _jid) {
        this._jid = _jid;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public Messages(String jid, String message, String state, Long time) {
        this._message = message;
        this._jid = jid;
        this._state = state;
        this._time = time;
    }
    public Messages(String jid, String message, String state, Long time,String host) {
        this._message = message;
        this._jid = jid;
        this._state = state;
        this._time = time;
        this._host=host;
    }

    public Messages(String jid, String message) {
        this._message = message;
        this._jid = jid;
    }
}
