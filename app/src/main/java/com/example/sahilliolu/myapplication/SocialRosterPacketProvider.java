package com.example.sahilliolu.myapplication;


import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.provider.IQProvider;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;


public class SocialRosterPacketProvider extends IQProvider<SocialRosterPacket> {

    public static final SocialRosterPacketProvider INSTANCE = new SocialRosterPacketProvider();

    private SocialRosterPacketProvider() {
    }

    @Override
    public SocialRosterPacket parse(XmlPullParser parser, int initialDepth) throws XmlPullParserException, IOException, SmackException {
        SocialRosterPacket roster = new SocialRosterPacket();

        outerloop: while (true) {
            int eventType = parser.next();
            switch(eventType) {
                case XmlPullParser.START_TAG:
                    String startTag = parser.getName();
                    switch (startTag) {
                        case "item":
                            roster.addRosterItem(parseItem(parser));
                            break;
                    }
                    break;
                case XmlPullParser.END_TAG:
                    String endTag = parser.getName();
                    switch(endTag) {
                        case "query":
                            break outerloop;
                    }
            }
        }
        return roster;
    }

    private SocialRosterPacket.Item parseItem(XmlPullParser parser)
            throws XmlPullParserException, IOException {
        boolean done = false;
        String userJID = parser.getAttributeValue("", "jid");
        String name = null;
        String username = null;
        while (!done) {
            int eventType = parser.next();
            if (eventType == XmlPullParser.START_TAG){
                String startTag = parser.getName();
                switch (startTag) {
                    case "name":
                        while (parser.next() != XmlPullParser.TEXT);
                        name = parser.getText();
                        break;
                    case "username":
                        while (parser.next() != XmlPullParser.TEXT);
                        username = parser.getText();
                        break;
                }
            } else if (eventType == XmlPullParser.END_TAG) {
                if (parser.getName().equals("item")) {
                    done = true;
                }
            }
        }
        return new SocialRosterPacket.Item(userJID, name, username);
    }
}
