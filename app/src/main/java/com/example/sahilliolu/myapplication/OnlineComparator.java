package com.example.sahilliolu.myapplication;

import java.util.Comparator;

/**
 * Created by sahillioğlu on 28.12.2016.
 */

public class OnlineComparator implements Comparator<Conversation> {
    @Override
    public int compare(Conversation b, Conversation a) {
        String value1="0";
        String value2="0";
        if(a.getContact().getIsOnline()){
            value1="1";
        }
        if(b.getContact().getIsOnline()){
            value2="1";
        }
        return value1.compareTo(value2);
    }
}
