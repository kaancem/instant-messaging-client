package com.example.sahilliolu.myapplication;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sahillioğlu on 28.12.2016.
 */

public class ConversationActivity extends AppCompatActivity {
    private static final String TAG = "ConversationActivity";

    private String currentToastMessage = "";
    private RecyclerView conversationRecyclerView;
    private ConversationAdapter mAdapter;
    private BroadcastReceiver mBroadcastReceiver = null;
    private ConversationModel conversationModel;
    private boolean initialPresencesProcessed = false;
    private ArrayList<ArrayList<String>> initialPresenceQueue = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialPresencesProcessed = false;
        setContentView(R.layout.activity_contact_list);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setIcon(R.mipmap.ic_launcher);
        setTitle(" Scorp Friends");

        conversationRecyclerView = (RecyclerView) findViewById(R.id.contact_list_recycler_view);
        conversationRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));

        conversationModel = ConversationModel.get(getBaseContext());
        mAdapter = new ConversationAdapter(conversationModel.getConversations());
        conversationRecyclerView.setAdapter(mAdapter);
    }
    @Override
    public void onBackPressed() {
        Intent setIntent = new Intent(Intent.ACTION_MAIN);
        setIntent.addCategory(Intent.CATEGORY_HOME);
        setIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(setIntent);
    }

    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        SharedPreferences sharedPreferences= PreferenceManager.getDefaultSharedPreferences(this);

        int id=item.getItemId();
        if(id == R.id.action_settings){
            Intent newIntent=new Intent(this,LoginActivity.class);
            try{
                sendBroadcast(new Intent(ConnectionService.SEND_LOGOUT));
                startActivity(newIntent);
                sharedPreferences.edit()
                        .putBoolean("xmpp_logged_in", false);// mJidView.getText().toString())
            }catch(Exception e){
                Toast.makeText(getApplicationContext(), "A problem occured.", Toast.LENGTH_SHORT).show();
                e.printStackTrace();
            }
        }
        return true;
    }

    private class ConversationHolder extends RecyclerView.ViewHolder {
        private TextView contactTextView;
        private Conversation mConversation;
        private Button mOnlineButton;
        public ConversationHolder(View itemView) {
            super(itemView);

            contactTextView = (TextView) itemView.findViewById(R.id.contact_jid);
            mOnlineButton=  (Button)    itemView.findViewById(R.id.message_button);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Inside here we start the chat activity
                    Intent intent = new Intent(ConversationActivity.this
                            , ChatActivity.class);
                    intent.putExtra("EXTRA_CONTACT_JID", mConversation.getContact().getJid());
                    intent.putExtra("EXTRA_CONTACT_REALNAME", mConversation.getContact().getRealName());
                    intent.putExtra("EXTRA_CONTACT_USERNAME", mConversation.getContact().getUserName());
                    startActivity(intent);
                }
            });
        }

        public void bindContact(Conversation conversation) {
            mConversation = conversation;
            if (mConversation == null) {
                return;
            }

            contactTextView.setText(findBestName(mConversation.getContact()));

            if(mConversation.getContact().getIsOnline()){
                mOnlineButton.setText("online");
                mOnlineButton.setTextColor(Color.rgb(0,100,0));
            }else{
                mOnlineButton.setText("offline");
                mOnlineButton.setTextColor(Color.RED);
            }
        }
    }

    @Override
    protected void onStart(){
        super.onStart();
        if (!isBroadcastActive())
            setUpBroadcastReceiver();

        // Get active roster
        sendBroadcast(new Intent(ConnectionService.SEND_ROSTER));

        // Get offline messages
        // sendBroadcast(new Intent(ConnectionService.SEND_OFFLINE_MESSAGES));
    }

    @Override
    protected void onResume(){
        super.onStart();
        if (!isBroadcastActive())
            setUpBroadcastReceiver();
    }

    @Override
    protected void onStop(){
        super.onStop();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        if (isBroadcastActive())
            destroyBroadcastReceiver();

        conversationModel.clearConversations();
        mAdapter.refreshConversations(conversationModel.getConversations());
    }

    protected boolean isBroadcastActive(){
        return mBroadcastReceiver != null;
    }

    private void setUpBroadcastReceiver(){
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (action.equals(ConnectionService.GET_ROSTER)){
                    ArrayList<Contact> contacts = intent.getParcelableArrayListExtra(ConnectionService.BUNDLE_CONTANCTS);
                    boolean changeOccurred = conversationModel.refreshContactsButKeepStatus(contacts);
                    if (changeOccurred) {
                        mAdapter.refreshConversations(conversationModel.getConversations());
                    }

                    if (!initialPresencesProcessed)
                        applyInitialPresence();

                } else if (action.equals(ConnectionService.NEW_PRESENCE)) {
                    String JID = intent.getStringExtra(ConnectionService.BUNDLE_FROM_JID);
                    String status = intent.getStringExtra(ConnectionService.BUNDLE_STATUS);
                    if (initialPresencesProcessed)
                        applyPresence(JID, status);
                    else {
                        ArrayList<String> presenceInfo = new ArrayList<>();
                        presenceInfo.add(JID);
                        presenceInfo.add(status);
                        initialPresenceQueue.add(presenceInfo);
                    }
                } else if (action.equals(ConnectionService.NEW_MESSAGE)){
                    String JID = intent.getStringExtra(ConnectionService.BUNDLE_FROM_JID);
                    String message = intent.getStringExtra(ConnectionService.BUNDLE_MESSAGE_BODY);

                    if(JID != null && message != null){
                        showToastForNewMessage(JID, message);
                        sendNewMessageNotification(JID, message);
                    }
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectionService.GET_ROSTER);
        filter.addAction(ConnectionService.NEW_PRESENCE);
        filter.addAction(ConnectionService.NEW_MESSAGE);

        registerReceiver(mBroadcastReceiver, filter);
    }

    private void destroyBroadcastReceiver(){
        unregisterReceiver(mBroadcastReceiver);
        mBroadcastReceiver = null;
    }

    private void applyInitialPresence(){
        for(ArrayList<String> presenceInfo: initialPresenceQueue){
            if(presenceInfo.size() >= 2)
                applyPresence(presenceInfo.get(0), presenceInfo.get(1));
        }
        initialPresenceQueue.clear();
        initialPresencesProcessed = true;
    }

    private void applyPresence(String JID, String status){
        boolean statusChanged = conversationModel.updateConversationWithStatus(JID, status.equals(ConnectionService.PRESENCE_ONLINE));
        if (statusChanged) {
            Log.d(TAG, "Status changed: " + JID);
            mAdapter.refreshConversations(conversationModel.getConversations());
        }
    }

    private void sendNewMessageNotification(String fromJID, String messageBody){
        if (messageBody == null)
            return;
        if(!ChatActivity.active || (ChatActivity.lastContactJID != null && fromJID != null && !ChatActivity.lastContactJID.equals(fromJID))) {
            String bestName = fromJID;
            if (fromJID != null) {
                Contact contact = conversationModel.getConversationByJID(fromJID).getContact();
                if(contact != null)
                    bestName = findBestName(contact);
            }

            Intent thisIntent = new Intent(this, ChatActivity.class);
            thisIntent.putExtra("EXTRA_CONTACT_JID", fromJID);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, thisIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            NotificationCompat.Builder b = new NotificationCompat.Builder(this);

            b.setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.drawable.fab_bg_mini)
                    .setTicker("Hearty365")
                    .setContentTitle("New message")
                    .setContentText("New message from " + bestName + ": " + messageBody)
                    .setContentIntent(contentIntent)
                    .setContentInfo("Info");
            b.setDefaults(Notification.DEFAULT_LIGHTS | Notification.DEFAULT_SOUND);

            NotificationManager notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(fromJID.hashCode(), b.build());
        }
    }

    private void showToastForNewMessage(String contactJID, String messageBody) {
        if(!ChatActivity.active || (ChatActivity.lastContactJID != null && contactJID != null && !ChatActivity.lastContactJID.equals(contactJID))){
            String bestName = contactJID;
            if (contactJID != null) {
                Contact contact = conversationModel.getConversationByJID(contactJID).getContact();
                if (contact != null)
                    bestName = findBestName(contact);
            }
            if (messageBody != null){
                currentToastMessage = "New message from " + bestName + ": " + messageBody;
                showToast();
            }
        }
    }

    private String findBestName(Contact contact){
        String bestName = contact.getJid();
        if(contact.getRealName() != null && (!contact.getRealName().equals("")))
            bestName = contact.getRealName();
        else if (contact.getUserName() != null)
            bestName = contact.getUserName();
        return bestName;
    }

    private void showToast(){
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(ConversationActivity.this, currentToastMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    private class ConversationAdapter extends RecyclerView.Adapter<ConversationHolder>
    {
        private List<Conversation> mConversation;

        public ConversationAdapter( List<Conversation> conversationList)
        {
            mConversation = new ArrayList<Conversation>();
            mConversation.addAll(conversationList);
        }

        @Override
        public ConversationHolder onCreateViewHolder(ViewGroup parent, int viewType) {

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater
                    .inflate(R.layout.list_item_contact, parent,
                            false);
            return new ConversationActivity.ConversationHolder(view);
        }


        @Override
        public void onBindViewHolder(ConversationHolder holder, int position) {
            Conversation conversation = mConversation.get(position);
            holder.bindContact(conversation);
        }

        public void refreshConversations(ArrayList<Conversation> newConversations){
            mConversation.clear();
            mConversation.addAll(newConversations);

            notifyDataSetChanged();
        }

        @Override
        public int getItemCount() {
            return mConversation.size();
        }
    }
}

