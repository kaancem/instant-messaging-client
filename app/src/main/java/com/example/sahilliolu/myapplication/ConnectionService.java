package com.example.sahilliolu.myapplication;

/**
 * Created by sahillioğlu on 17.11.2016.
 */



import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by gakwaya on 4/28/2016.
 */
public class ConnectionService extends Service {
    private static final String TAG ="RoosterService";

    /* INTENT ACTIONS USED IN BROADCASTS */
    public static final String UI_AUTHENTICATED = "com.example.sahilliolu.rooster.uiauthenticated";
    public static final String SEND_MESSAGE = "com.example.sahilliolu.rooster.sendmessage";
    public static final String GET_ROSTER = "com.example.sahilliolu.rooster.getroster";
    public static final String SEND_ROSTER = "com.example.sahilliolu.rooster.sendroster";
    public static final String SEND_OFFLINE_MESSAGES = "com.example.sahilliolu.rooster.offlinemesages";
    public static final String SEND_LOGOUT = "com.example.sahilliolu.rooster.logout";

    public static final String BUNDLE_MESSAGE_BODY = "b_body";
    public static final String BUNDLE_TO_JID = "b_to";
    public static final String BUNDLE_FROM_JID = "b_from";
    public static final String BUNDLE_STATUS = "b_status";
    public static final String BUNDLE_CONTANCTS = "b_contacts";
    public static final String NEW_PRESENCE = "com.example.sahilliolu.newpresence";
    public static final String NEW_MESSAGE = "com.example.sahilliolu.newmessage";

    public static final String PRESENCE_ONLINE = "online";
    public static final String PRESENCE_OFFLINE = "offline";

    public static Connection.ConnectionState sConnectionState;
    public static Connection.LoggedInState sLoggedInState;
    private boolean mActive; //Stores whether or not the thread is active
    private Thread mThread;
    private Handler mTHandler;  //We use this handler to post messages to
    //the background thread.
    private Connection mConnection;

    public ConnectionService() {

    }
    public static Connection.ConnectionState getState()
    {
        if (sConnectionState == null)
        {
            return Connection.ConnectionState.DISCONNECTED;
        }
        return sConnectionState;
    }

    public static Connection.LoggedInState getLoggedInState()
    {
        if (sLoggedInState == null)
        {
            return Connection.LoggedInState.LOGGED_OUT;
        }
        return sLoggedInState;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private void initConnection()
    {
        if( mConnection == null)
        {
            mConnection = new Connection(this);
        }
        try
        {
            mConnection.connect();

        }catch (IOException | SmackException | XMPPException e)
        {
            Log.d(TAG,"Something went wrong while connecting, make sure the credentials are right and try again");
            e.printStackTrace();
            //Stop the service all together.
            stopSelf();
        }
    }


    public void start()
    {
        if(!mActive)
        {
            mActive = true;
            if( mThread ==null || !mThread.isAlive())
            {
                mThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        mTHandler = new Handler();
                        initConnection();
                        //THE CODE HERE RUNS IN A BACKGROUND THREAD.
                        Looper.loop();
                    }
                });
                mThread.start();
            }
        }

    }

    public void stop()
    {
        mActive = false;
        mTHandler.post(new Runnable() {
            @Override
            public void run() {
                if( mConnection != null)
                {
                    mConnection.disconnect();
                }
            }
        });
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        start();
        return Service.START_STICKY;
        //RETURNING START_STICKY CAUSES OUR CODE TO STICK AROUND WHEN THE APP ACTIVITY HAS DIED.
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stop();
    }
}
