package com.example.sahilliolu.myapplication;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * Created by sahillioğlu on 28.12.2016.
 */

public class ConversationModel {

    private static ConversationModel sContactModel;
    private ArrayList<Conversation> mConversation;

    public static ConversationModel get(Context context)
    {
        if(sContactModel == null)
        {
            sContactModel = new ConversationModel(context);
        }
        return  sContactModel;
    }

    private ConversationModel(Context context)
    {
        mConversation = new ArrayList<>();
        populateWithInitialContacts(context);
    }

    private void populateWithInitialContacts(Context context)
    {
        // No initial contacts.
    }

    private void sortContacts() {
        Collections.sort(mConversation, new NameComparator());
        Collections.sort(mConversation, new OnlineComparator());
    }

    public ArrayList<Conversation> getConversations()
    {
        return mConversation;
    }

    public void clearConversations(){
        mConversation.clear();
    }

    public boolean updateConversationWithStatus(String jid, Boolean onlineStatus){
        for(Conversation conversation: mConversation){
            if(conversation.getContact().getJid().equals(jid)){
                conversation.getContact().setIsOnline(onlineStatus);
                sortContacts();
                return true;
            }
        }
        return false;
    }

    public boolean refreshContactsButKeepStatus(List<Contact> contacts){
        HashMap<String, Boolean> onlineLookup = new HashMap<>();
        for(Conversation conversation: mConversation){
            Contact currentContact = conversation.getContact();
            onlineLookup.put(currentContact.getJid(), currentContact.getIsOnline());
        }

        ArrayList<Conversation> newConversations = new ArrayList<>();
        for(Contact contact: contacts){
            String JID = contact.getJid();
            boolean isOnline = false;
            if (onlineLookup.containsKey(JID))
                isOnline = onlineLookup.get(JID);

            newConversations.add(new Conversation(contact.withOnlineStatus(isOnline)));
        }

        mConversation = newConversations;
        sortContacts();
        return true;
    }

    public Conversation getConversationByJID(String contactJID){
        for(Conversation conversation: mConversation) {
            Contact currentContact = conversation.getContact();
            if (currentContact.getJid().equals(contactJID)) {
                return conversation;
            }
        }
        return null;
    }
}
