package com.example.sahilliolu.myapplication;

import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by sahillioğlu on 28.12.2016.
 */



public class SocialRosterPacket extends IQ {
    public static final String ELEMENT = QUERY_ELEMENT;
    public static final String NAMESPACE = "jabber:iq:social:roster";

    private final List<SocialRosterPacket.Item> rosterItems = new ArrayList<SocialRosterPacket.Item>();

    private String authType;
    private String auth;

    public SocialRosterPacket() {
        super(ELEMENT, NAMESPACE);
        this.authType = "token";
        this.auth = "";
    }

    public SocialRosterPacket(String authType, String auth){
        super(ELEMENT, NAMESPACE);
        this.authType = authType;
        this.auth = auth;
    }

    public void addRosterItem(SocialRosterPacket.Item item) {
        synchronized (rosterItems) {
            rosterItems.add(item);
        }
    }

    public int getRosterItemCount() {
        synchronized (rosterItems) {
            return rosterItems.size();
        }
    }

    public List<SocialRosterPacket.Item> getRosterItems() {
        synchronized (rosterItems) {
            return new ArrayList<SocialRosterPacket.Item>(rosterItems);
        }
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();
        xml.halfOpenElement("auth");
        xml.attribute("type", this.authType);
        xml.rightAngleBracket();
        xml.append(auth);
        xml.closeElement("auth");

        synchronized (rosterItems) {
            for (SocialRosterPacket.Item entry : rosterItems) {
                xml.append(entry.toXML());
            }
        }
        return xml;
    }

    public static class Item {

        private String userJID;
        private String name;
        private String username;

        public Item(String userJID, String name, String username) {
            this.userJID = userJID.toLowerCase(Locale.US);
            this.name = name;
            this.username = username;
        }

        public String getUserJID(){
            return this.userJID;
        }

        public void setUserJID(String userJID){
            this.userJID = userJID;
        }

        public String getName(){
            return this.name;
        }

        public void setName(String name){
            this.name = name;
        }

        public String getUsername(){
            return this.username;
        }

        public void setUsername(String username){
            this.username = username;
        }

        public XmlStringBuilder toXML() {
            XmlStringBuilder xml = new XmlStringBuilder();
            xml.halfOpenElement(Packet.ITEM).attribute("jid", userJID);
            xml.rightAngleBracket();

            xml.optElement("name", name);
            xml.optElement("username", username);

            xml.closeElement(Packet.ITEM);
            return xml;
        }
    }
}
