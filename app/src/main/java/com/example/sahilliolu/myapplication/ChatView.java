package com.example.sahilliolu.myapplication;

/**
 * Created by sahillioğlu on 15.12.2016.
 */

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import co.devcenter.androiduilibrary.ChatViewEventListener;
import co.devcenter.androiduilibrary.ChatViewListAdapter;
import co.devcenter.androiduilibrary.SendButton;
import co.devcenter.androiduilibrary.models.ChatMessage;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import co.devcenter.androiduilibrary.models.ChatMessage;
import co.devcenter.androiduilibrary.models.ChatMessage.Status;

/**
 * Created by timi on 17/11/2015.
 */
public class ChatView extends LinearLayout {

    private ChatViewListAdapter chatViewListAdapter;
    private ListView chatListView;

    private EditText inputEditText;
    private SendButton sendButton;
    private ChatViewEventListener eventListener;
    private boolean previousFocusStatus = false;


    public ChatView(Context context){
        super(context);
        init(context);
    }

    public ChatView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ChatView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }



    private void init(Context context){
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(co.devcenter.androiduilibrary.R.layout.chat_view, this);

        chatListView = (ListView)findViewById(co.devcenter.androiduilibrary.R.id.chat);
        inputEditText = (EditText)findViewById(co.devcenter.androiduilibrary.R.id.inputEditText);
        sendButton = (SendButton) findViewById(co.devcenter.androiduilibrary.R.id.sendButton);

        chatViewListAdapter = new ChatViewListAdapter(context);
        chatListView.setAdapter(chatViewListAdapter);
    }


    public void setEventListener(final ChatViewEventListener eventListener){
        this.eventListener = eventListener;
        setUserTypingListener();
        setUserStoppedTypingListener();
    }

    public String getTypedString(){
        return  inputEditText.getText().toString();
    }

    public void sendMessage(ChatMessage message){
        chatViewListAdapter.addMessage(message);
        inputEditText.setText("");
    }

    public void sendMessage(){
        ChatMessage chatMessage = new ChatMessage(inputEditText.getText().toString(), System.currentTimeMillis(), ChatMessage.Status.SENT);
        inputEditText.setText("");
        chatViewListAdapter.addMessage(chatMessage);
    }

    public void receiveMessage(String message){
        ChatMessage chatMessage = new ChatMessage(message, System.currentTimeMillis(), ChatMessage.Status.RECEIVED);
        chatViewListAdapter.addMessage(chatMessage);
    }
    public void receiveMessage(ChatMessage message){
        chatViewListAdapter.addMessage(message);
    }

    public SendButton getSendButton(){
        return sendButton;
    }

    private void setUserTypingListener(){
        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0){
                    eventListener.userIsTyping();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void setUserStoppedTypingListener(){
        inputEditText.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    previousFocusStatus = true;
                } else if (!hasFocus) {
                    previousFocusStatus = false;
                } else if (previousFocusStatus && !hasFocus) {
                    eventListener.userHasStoppedTyping();
                }
            }
        });
    }
}

