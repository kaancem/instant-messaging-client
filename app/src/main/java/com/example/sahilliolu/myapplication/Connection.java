package com.example.sahilliolu.myapplication;

/**
 * Created by sahillioğlu on 17.11.2016.
 */

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ChatMessageListener;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketCollector;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.OrFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.offline.OfflineMessageManager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created by gakwaya on 4/28/2016.
 */
public class Connection implements ConnectionListener, ChatMessageListener {

    private static final String TAG = "Connection";
    private final Context mApplicationContext;
    private final String mUsername;
    private String mPassword;
    private final String mServiceName;
    private final String mUserData;
    private final String mUserJID;
    private XMPPTCPConnection mConnection;
    private PacketCollector mCollector;
    private boolean readPacketsAllowed = true;
    private BroadcastReceiver uiThreadMessageReceiver;  //Receives messages from the ui thread.
    EndDB endDb;

    public enum ConnectionState
    {
        CONNECTED , AUTHENTICATED, CONNECTING, DISCONNECTING ,DISCONNECTED;
    }

    public enum LoggedInState
    {
        LOGGED_IN, LOGGED_OUT;
    }

    public Connection(Context context)
    {
        mApplicationContext = context.getApplicationContext();
        endDb=new EndDB(mApplicationContext,null,null,1);
        String username = PreferenceManager.getDefaultSharedPreferences(mApplicationContext)
                .getString("backend_id", null);

        if( username != null)
        {
            mUsername = username;
            mServiceName = "im-dev.scorpapp.com";
            mPassword = PreferenceManager.getDefaultSharedPreferences(mApplicationContext).getString("xmpp_password",null);
            mUserData = PreferenceManager.getDefaultSharedPreferences(mApplicationContext).getString("xmpp_jid",null);
            mUserJID = username + "@" + mServiceName;
        }else
        {
            mUsername = "";
            mServiceName = "";
            mUserData = "";
            mUserJID = "";
        }
        ProviderManager.addIQProvider(SocialRosterPacket.ELEMENT, SocialRosterPacket.NAMESPACE, SocialRosterPacketProvider.INSTANCE);
    }

    public void connect() throws IOException,XMPPException,SmackException
    {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Log.d(TAG, "Connecting to server " + mServiceName);
        XMPPTCPConnectionConfiguration.XMPPTCPConnectionConfigurationBuilder builder=
                XMPPTCPConnectionConfiguration.builder();
        builder.setServiceName(mServiceName);
       // builder.setHost(mServiceName);
        builder.setUsernameAndPassword(mUsername, mPassword);
        builder.setDebuggerEnabled(true);
        builder.setRosterLoadedAtLogin(false);
        builder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled)
            .setPort(5222);

        mConnection = new XMPPTCPConnection(builder.build());
        mConnection.addConnectionListener(this);
        mConnection.connect();
        mConnection.login();

        //Set up the ui thread broadcast message receiver.
        setupUIThreadBroadCastMessageReceiver();

        PacketFilter filter = new OrFilter(
                new PacketTypeFilter(Message.class),
                new PacketTypeFilter(Presence.class),
                new PacketTypeFilter(SocialRosterPacket.class));
        mCollector = mConnection.createPacketCollector(filter);

        readPacketsAllowed = true;
        listenPackets();
    }

    public void disconnect()
    {
        Log.d(TAG,"Disconnecting from server " + mServiceName);
        readPacketsAllowed = false;
        try
        {
            if (mConnection != null)
            {
                mConnection.disconnect();
            }

        }catch (SmackException.NotConnectedException e)
        {
            ConnectionService.sConnectionState=ConnectionState.DISCONNECTED;
            e.printStackTrace();
        }

        mConnection = null;

        // Unregister the message broadcast receiver.
        if( uiThreadMessageReceiver != null)
        {
            mApplicationContext.unregisterReceiver(uiThreadMessageReceiver);
            uiThreadMessageReceiver = null;
        }
    }

    private void setupUIThreadBroadCastMessageReceiver()
    {
        uiThreadMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //Check if the Intents purpose is to send the message.
                String action = intent.getAction();
                if( action.equals(ConnectionService.SEND_MESSAGE))
                {
                    //Send the message.
                    sendMessage(intent.getStringExtra(ConnectionService.BUNDLE_MESSAGE_BODY),
                            intent.getStringExtra(ConnectionService.BUNDLE_TO_JID));
                } else if (action.equals(ConnectionService.SEND_ROSTER)) {
                    // Send get roster packet
                    sendGetRoster();
                } else if (action.equals(ConnectionService.SEND_OFFLINE_MESSAGES)){
                    // Send offline messages packet
                    sendGetOfflineMessages();
                } else if (action.equals(ConnectionService.SEND_LOGOUT)){
                    readPacketsAllowed = false;
                    mApplicationContext.stopService(new Intent(mApplicationContext, ConnectionService.class));
                }
            }
        };

        IntentFilter filter = new IntentFilter();
        filter.addAction(ConnectionService.SEND_MESSAGE);
        filter.addAction(ConnectionService.SEND_ROSTER);
        filter.addAction(ConnectionService.SEND_OFFLINE_MESSAGES);
        filter.addAction(ConnectionService.SEND_LOGOUT);
        mApplicationContext.registerReceiver(uiThreadMessageReceiver, filter);
    }

    private void sendMessage ( String body ,String toJid)
    {
        if (body != null && body.length() > 0) {
            Log.d(TAG, "Sending message to :" + toJid);
            Chat chat = ChatManager.getInstanceFor(mConnection).createChat(toJid, this);
            try {
                chat.sendMessage(body);
                Messages testEnd = new Messages(toJid, body, "sent", System.currentTimeMillis(), mUserData);
                endDb.addnewMessage(testEnd);
            } catch (SmackException.NotConnectedException | XMPPException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendGetRoster(){
        SocialRosterPacket iq = new SocialRosterPacket("token", mPassword);
        iq.setType(IQ.Type.get);

        try {
            mConnection.sendPacket(iq);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }

    }

    private void sendGetOfflineMessages(){
        OfflineMessageManager offlineManager = new OfflineMessageManager(mConnection);
        List<Message> offlineMessages = null;
        try {
            offlineMessages = offlineManager.getMessages();
        } catch (SmackException.NoResponseException e) {
            e.printStackTrace();
        } catch (XMPPException.XMPPErrorException e) {
            e.printStackTrace();
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
        if (offlineMessages != null){
            Map<String,ArrayList<Message>> offlineMsgs = new HashMap<String,ArrayList<Message>>();
            for (Message message: offlineMessages) {
                String fromUser = getRealJID(message.getFrom());
                if(offlineMsgs.containsKey(fromUser))
                {
                    offlineMsgs.get(fromUser).add(message);
                }else{
                    ArrayList<Message> temp = new ArrayList<Message>();
                    temp.add(message);
                    offlineMsgs.put(fromUser, temp);
                }
            }
            try {
                offlineManager.deleteMessages();
            } catch (SmackException.NoResponseException e) {
                e.printStackTrace();
            } catch (XMPPException.XMPPErrorException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            }
        }
    }

    public void listenPackets() {
        while (readPacketsAllowed) {
            Packet packet = mCollector.nextResult();
            Log.d(TAG, "listenPackets: new packet");
            if(packet != null) {
                if (packet instanceof Presence)
                    handleNewPresence((Presence) packet);
                else if (packet instanceof Message)
                    handleNewMessage((Message) packet);
                else if (packet instanceof SocialRosterPacket) {
                    handleSocialRosterResult((SocialRosterPacket) packet);
                } else
                    Log.d(TAG, "Unrecognized packet type received.");
            }
        }
        mCollector.cancel();
    }

    private boolean handleNewPresence(Presence presence){
        String contactJid = getRealJID(presence.getFrom());
        if (contactJid != null && !contactJid.equals(mUserJID)) {
            if (presence.isAvailable()) {
                Log.d(TAG, contactJid + " is available.");
                broadcastPresence(contactJid, ConnectionService.PRESENCE_ONLINE);
            } else {
                Log.d(TAG, contactJid + " has gone offline.");
                broadcastPresence(contactJid, ConnectionService.PRESENCE_OFFLINE);
            }
            return true;
        } else {
            return false;
        }
    }

    private boolean handleNewMessage(Message message) {
        String contactJID = getRealJID(message.getFrom());
        String messageBody = message.getBody();
        if (contactJID != null && messageBody != null) {
            System.out.println("Received message from "
                    + contactJID + " : "
                    + message.getBody());

            saveNewMessageToDB(contactJID, messageBody);
            broadcastNewMessage(contactJID, messageBody);
            return true;
        } else
            return false;
    }

    private void handleSocialRosterResult(SocialRosterPacket rosterPacket){
        Log.d(TAG, "SocialRosterPacket result received.");

        ArrayList<Contact> contacts = new ArrayList<Contact>();
        for(SocialRosterPacket.Item item: rosterPacket.getRosterItems()){
            contacts.add(new Contact(item.getUserJID(), item.getUsername(), item.getName()));
        }

        Intent intent = new Intent(ConnectionService.GET_ROSTER);
        intent.setPackage(mApplicationContext.getPackageName());
        intent.putExtra(ConnectionService.BUNDLE_CONTANCTS, contacts);
        mApplicationContext.sendBroadcast(intent);
    }

    private void showContactListActivityWhenAuthenticated()
    {
        Intent i = new Intent(ConnectionService.UI_AUTHENTICATED);
        i.setPackage(mApplicationContext.getPackageName());
        mApplicationContext.sendBroadcast(i);
        Log.d(TAG,"Sent the broadcast that we are authenticated");
    }

    private String getRealJID(String JID){
        if (JID != null && JID.contains("/")) {
            return JID.split("/")[0];
        } else {
            return JID;
        }
    }

    private void broadcastPresence(String JID, String status){
        Intent intent = new Intent(ConnectionService.NEW_PRESENCE);
        intent.setPackage(mApplicationContext.getPackageName());
        intent.putExtra(ConnectionService.BUNDLE_FROM_JID, JID);
        intent.putExtra(ConnectionService.BUNDLE_STATUS, status);
        mApplicationContext.sendBroadcast(intent);
    }

    private void saveNewMessageToDB(String fromJID, String messageBody){
        Messages endTest = new Messages(fromJID, messageBody, "received", System.currentTimeMillis(), mUserData);
        endDb.addnewMessage(endTest);
    }

    private void broadcastNewMessage(Message message){
        broadcastNewMessage(getRealJID(message.getFrom()), message.getBody());
    }

    private void broadcastNewMessage(String fromJID, String messageBody){
        Intent intent = new Intent(ConnectionService.NEW_MESSAGE);
        intent.setPackage(mApplicationContext.getPackageName());
        intent.putExtra(ConnectionService.BUNDLE_FROM_JID, fromJID);
        intent.putExtra(ConnectionService.BUNDLE_MESSAGE_BODY, messageBody);
        mApplicationContext.sendBroadcast(intent);
    }

    @Override
    public void processMessage(Chat chat, Message message) {
        // handled in generic packet listening method listenPackets.
    }

    @Override
    public void connected(XMPPConnection connection) {
        ConnectionService.sConnectionState=ConnectionState.CONNECTED;
        Log.d(TAG,"Connected Successfully");
    }

    @Override
    public void authenticated(XMPPConnection connection) {
        ConnectionService.sConnectionState=ConnectionState.CONNECTED;
        Log.d(TAG,"Authenticated Successfully");
        showContactListActivityWhenAuthenticated();
    }

    @Override
    public void connectionClosed() {
        ConnectionService.sConnectionState=ConnectionState.DISCONNECTED;
        Log.d(TAG,"Connectionclosed()");
    }

    @Override
    public void connectionClosedOnError(Exception e) {
        ConnectionService.sConnectionState=ConnectionState.DISCONNECTED;
        Log.d(TAG,"ConnectionClosedOnError, error "+ e.toString());

    }

    @Override
    public void reconnectingIn(int seconds) {
        ConnectionService.sConnectionState = ConnectionState.CONNECTING;
        Log.d(TAG,"ReconnectingIn() ");
    }

    @Override
    public void reconnectionSuccessful() {
        ConnectionService.sConnectionState = ConnectionState.CONNECTED;
        Log.d(TAG,"ReconnectionSuccessful()");
    }

    @Override
    public void reconnectionFailed(Exception e) {
        ConnectionService.sConnectionState = ConnectionState.DISCONNECTED;
        Log.d(TAG,"ReconnectionFailed()");
    }
}