package com.example.sahilliolu.myapplication;

/**
 * Created by sahillioğlu on 17.11.2016.
*/


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import co.devcenter.androiduilibrary.ChatViewEventListener;
import co.devcenter.androiduilibrary.SendButton;
import co.devcenter.androiduilibrary.models.ChatMessage;

//import co.devcenter.androiduilibrary.ChatView;


public class ChatActivity extends AppCompatActivity {
    private static final String TAG ="ChatActivity";
    private  Context mApplicationContext;
    private String contactJid;
    private String userName;
    private String realName;
    private ChatView mChatView;
    private SendButton mSendButton;
    private String friendName;
    private BroadcastReceiver mBroadcastReceiver;
    static boolean active = false;
    static String lastContactJID = "";
    EndDB endDb;
    UserInfo userInfo;
    ArrayList<Messages> screenMessages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        int j=0;
        endDb=new EndDB(this,null,null,1);
        userInfo=new UserInfo(this,null,null,1);
        String[] infos=new String[1000];
        try {
            userInfo.holdMessage(infos);
        }catch(Exception e){}
        for(int i=0;i<infos.length;i++){
            if(! (infos[i]==null)){
                j=i;
            }
        }
        userName=infos[j];
        mChatView = (ChatView) findViewById(R.id.chat_view);
        Intent intent = getIntent();
        contactJid = intent.getStringExtra("EXTRA_CONTACT_JID");
        realName = intent.getStringExtra("EXTRA_CONTACT_REALNAME");
        friendName = intent.getStringExtra("EXTRA_CONTACT_USERNAME");
        if(realName != null && !realName.equals(""))
            setTitle(realName);
        else
            setTitle(friendName);

        mChatView.setEventListener(new ChatViewEventListener() {
            @Override
            public void userIsTyping() {
                //Here you know that the user is typing
            }

            @Override
            public void userHasStoppedTyping() {
                //Here you know that the user has stopped typing.
            }
        });

        mSendButton = mChatView.getSendButton();
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Only send the message if the client is connected
                //to the server.

                if (ConnectionService.getState().equals(Connection.ConnectionState.CONNECTED)) {
                    //Send the message to the server

                    Intent intent = new Intent(ConnectionService.SEND_MESSAGE);
                    intent.putExtra(ConnectionService.BUNDLE_MESSAGE_BODY,
                            mChatView.getTypedString());
                    intent.putExtra(ConnectionService.BUNDLE_TO_JID, contactJid);
                    String hold=mChatView.getTypedString();
                    sendBroadcast(intent);

                    //Update the chat view.
                    mChatView.sendMessage();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Client not connected to server, Message not sent!",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
        lastContactJID = contactJid;
        refreshMessages();
        if (mBroadcastReceiver == null)
            setUpBroadcastReceiver();
    }
    @Override
    public void onStop() {
        super.onStop();
        active = false;
        if (mBroadcastReceiver != null)
            unregisterReceiver(mBroadcastReceiver);
        mBroadcastReceiver = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mBroadcastReceiver == null)
            setUpBroadcastReceiver();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void refreshMessages(){
        screenMessages.clear();
        endDb.holdMessage(screenMessages);
        for(Messages message: screenMessages) {
            if (!(message == null || message.get_message() == null||message.get_jid()==null)) {
                if (message.get_jid().equals(contactJid)) {
                    if (!message.get_message().trim().equals("")) {
                        if (message.get_host().equals(userName)) {
                            if (message.get_state().equals("sent")) {
                                mChatView.sendMessage(new ChatMessage(message.get_message(), message.getTime(), ChatMessage.Status.SENT));
                            } else if (message.get_state().equals("received")) {
                                mChatView.receiveMessage(new ChatMessage(message.get_message(), message.getTime(), ChatMessage.Status.RECEIVED));
                            }
                        }
                    }
                }
            }
        }
    }

    private void setUpBroadcastReceiver(){
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                switch (action)
                {
                    case ConnectionService.NEW_MESSAGE:
                        String from = intent.getStringExtra(ConnectionService.BUNDLE_FROM_JID);
                        String body = intent.getStringExtra(ConnectionService.BUNDLE_MESSAGE_BODY);

                        if (from.equals(contactJid))
                        {
                            if(body != null) {
                                mChatView.receiveMessage(body);
                            }
                        }
                        return;
                }
            }
        };
        IntentFilter filter = new IntentFilter(ConnectionService.NEW_MESSAGE);
        registerReceiver(mBroadcastReceiver,filter);
    }
}