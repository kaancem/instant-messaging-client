package com.example.sahilliolu.myapplication;

import java.util.Comparator;

/**
 * Created by sahillioğlu on 28.12.2016.
 */

public class NameComparator implements Comparator<Conversation> {
    @Override
    public int compare(Conversation a, Conversation b) {
        Contact x = a.getContact();
        Contact y = b.getContact();
        if (x.getRealName() == null || x.getRealName().equals("")) {
            if (y.getRealName() == null || y.getRealName().equals("")) {
                return x.getUserName().compareToIgnoreCase(y.getUserName());
            }
            return x.getUserName().compareToIgnoreCase(y.getRealName());}
        if(y.getRealName()==null || y.getRealName().equals("")){
            return x.getRealName().compareToIgnoreCase(y.getUserName());
        }
        return x.getRealName().compareToIgnoreCase(y.getRealName());
    }
}